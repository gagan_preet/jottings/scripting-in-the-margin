"""NOXFile."""
import tempfile
from typing import Any

import nox
from nox.sessions import Session


nox.options.sessions = "lint", "safety", "mypy", "pytype", "typeguard"
locations = "scripts", "noxfile.py", "docs/conf.py", "vscode_setup.py"
package = "scripting_in_the_margin"


def install_with_constraints(session: Session, *args: str, **kwargs: Any) -> None:
    """Install packages constrained by Poetry's lock file.

    This function is a wrapper for nox.sessions.Session.install. It
    invokes pip to install packages inside of the session's virtualenv.
    Additionally, pip is passed a constraints file generated from
    Poetry's lock file, to ensure that the packages are pinned to the
    versions specified in poetry.lock. This allows you to manage the
    packages as Poetry development dependencies.

    Arguments:
        session: The Session object.
        args: Command-line arguments for pip.
        kwargs: Additional keyword arguments for Session.install.
    """
    install_required(session)
    if len(args) > 0:
        with tempfile.NamedTemporaryFile() as requirements:
            session.run(
                "poetry",
                "export",
                "--dev",
                "--format=requirements.txt",
                f"--output={requirements.name}",
                external=True,
            )
            session.install(f"--constraint={requirements.name}", *args, **kwargs)


def install_required(session: Session) -> None:
    """Install required packages constrained by Poetry's lock file.

    This function is a wrapper for nox.sessions.Session.install. It
    invokes pip to install required packages inside of the session's virtualenv.
    Additionally, pip is passed a constraints file generated from
    Poetry's lock file, to ensure that the packages are pinned to the
    versions specified in poetry.lock. This allows you to manage the
    packages as Poetry dependencies.

    Arguments:
        session: The Session object.
    """
    session.run("poetry", "install", "--no-dev", external=True)
    with tempfile.NamedTemporaryFile() as requirements:
        session.run(
            "poetry",
            "export",
            "--format=requirements.txt",
            f"--output={requirements.name}",
            external=True,
        )
        first_installs = []  # [["cython"]]
        for fi in first_installs:
            session.install(f"--constraint={requirements.name}", *fi)
        session.install("-r", requirements.name)
    session.install(".", "--no-deps")


@nox.session(python="3.8")
def black(session: Session) -> None:
    """Run black code formatter."""
    args = session.posargs or locations
    install_with_constraints(session, "black")
    session.run("black", *args)


@nox.session(python=["3.8", "3.7"])
def lint(session: Session) -> None:
    """Lint using flake8."""
    args = session.posargs or locations
    install_with_constraints(
        session,
        "flake8",
        "flake8-annotations",
        "flake8-bandit",
        "flake8-black",
        "flake8-bugbear",
        "flake8-docstrings",
        "flake8-import-order",
        "darglint",
    )
    session.run("flake8", *args)


@nox.session(python="3.8")
def safety(session: Session) -> None:
    """Scan dependencies for insecure packages."""
    install_with_constraints(session, "safety")
    with tempfile.NamedTemporaryFile() as requirements:
        session.run(
            "poetry",
            "export",
            "--dev",
            "--format=requirements.txt",
            f"--output={requirements.name}",
            external=True,
        )
        session.run("safety", "check", f"--file={requirements.name}", "--full-report")


@nox.session(python=["3.8", "3.7"])
def mypy(session: Session) -> None:
    """Type-check using mypy."""
    args = session.posargs or locations
    install_with_constraints(session, "mypy")
    session.run("mypy", *args, external=True)


@nox.session(python="3.7")
def pytype(session: Session) -> None:
    """Type-check using pytype."""
    args = session.posargs or [
        "--disable=import-error",
        *[location for location in locations if location not in ["noxfile.py"]],
    ]
    install_with_constraints(session, "pytype")
    session.run("pytype", *args)


@nox.session(python=["3.8", "3.7"])
def typeguard(session: Session) -> None:
    """Runtime type checking using Typeguard."""
    args = session.posargs or ["-m", "not e2e"]
    install_with_constraints(session, "pytest", "pytest-mock", "typeguard")
    session.run(
        "pytest", f"--typeguard-packages={package}", *args, external=True,
    )


@nox.session(python=["3.8", "3.7"])
def xdoctest(session: Session) -> None:
    """Run examples with xdoctest."""
    args = session.posargs or ["all"]
    install_with_constraints(session, "xdoctest")
    session.run("python", "-m", "xdoctest", package, *args, external=True)


@nox.session(python="3.8")
def docs(session: Session) -> None:
    """Build the documentation."""
    install_with_constraints(
        session,
        "sphinx",
        "sphinx-autodoc-typehints",
        # "sphinx_rtd_theme"
        "sphinx_material",
    )
    session.run(
        "sphinx-apidoc",
        *[
            "-d",
            "5",
            "--separate",
            "--private",
            "--implicit-namespaces",
            "--module-first",
            "-o",
            "docs",
            "scripts",
        ],
    )
    session.run("sphinx-build", "docs", "docs/_build")
