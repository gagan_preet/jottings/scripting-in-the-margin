"""Scripting in the Margin.

This is a package that provides scripts based codebase for
`Jotting in the Margin <http://jotting.gagan-preet.com/>`_.

This module will be used as a base for the CLI.
"""
import click


__version__ = "0.1.0"


@click.command()
@click.version_option(version=__version__)
def main() -> None:
    """The Coding in the Margin project."""
    click.echo("Hello, world!")
