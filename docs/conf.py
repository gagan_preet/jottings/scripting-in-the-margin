"""Configuration for sphinx."""
project = "Scripting In The Margin"
copyright = "2020, Gagan Preet Singh <s.gagan.preet@gmail.com>"
author = "Gagan Preet Singh <s.gagan.preet@gmail.com>"


extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
]

exclude_patterns = []


html_theme = "sphinx_material"

html_theme_options = {
    "nav_title": project,
    # 'google_analytics_account': 'UA-XXXXX',
    "base_url": "https://scripting.gagan-preet.com",
    "color_primary": "blue",
    "color_accent": "light-blue",
    "repo_url": "https://gitlab.com/gagan_preet/jottings/scripting-in-the-margin",
    "repo_name": "Scripting in the Margin",
    "globaltoc_depth": 3,
    "globaltoc_collapse": False,
    "globaltoc_includehidden": True,
}
