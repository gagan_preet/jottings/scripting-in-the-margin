Welcome to Scripting In The Margin's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   license
   reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
